# project-oppa
Oppa-factor Authentication를 구현하는 데 사용되는 응용 프로그램

## 요구 사항:
* [Python 3](https://www.python.org/downloads/)
* [requests](https://2.python-requests.org/en/master/)

## 설치:
1. 이 저장소를 복제하십시오
2. 필요한 종속성을 설치하십시오
  * Linux / Mac의 경우: `install.sh`
  * Windows에서: `install-windows.bat`

## 용법:
1. Virtual Environment 활성화
  * Linux / Mac의 경우: `source venv/bin/activate`
  * Windows에서: `venv\Scripts\activate`
2. `python request_launch_code.py`를 실행하십시오
3. 권한이 있으면 사용자 이름과 암호를 제공하십시오
