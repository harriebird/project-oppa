import getpass
import requests

HOST_NAME = 'https://harriebird.pythonanywhere.com'
PROXIES = {

}


def get_csrf_token():
    try:
        response = requests.get(HOST_NAME, proxies=PROXIES)
        return response.cookies['csrftoken']
    except KeyError:
        print('---------------------------------------------------------------')
        print('죄송합니다.이 애플리케이션에 액세스 할 수있는 권한이 없습니다.')
        exit()


def login():
    username = str(input('사용자 이름: '))
    password = getpass.getpass(prompt='{}\'s 암호: '.format(username))
    token = get_csrf_token()
    headers = {'operation': 'get_key', 'Cookie': 'csrftoken={}'.format(token), 'Referer': HOST_NAME}
    data = {'username': username, 'password': password, 'csrfmiddlewaretoken': token}
    response = requests.post('{}/home/'.format(HOST_NAME), data=data, headers=headers, proxies=PROXIES)
    if response.status_code == 200:
        print('---------------------------------------------------------------')
        print(response.json()['message'])
        print('이 코드를 사용하여 수퍼 유저 컨트롤에 액세스 할 수 있습니다.')
        print('---------------------------------------------------------------')
    else:
        print('---------------------------------------------------------------')
        print(response.json()['error'])
        login()

print("""
         ▄██████▄     ▄███████▄    ▄███████▄    ▄████████
        ███    ███   ███    ███   ███    ███   ███    ███
        ███    ███   ███    ███   ███    ███   ███    ███
        ███    ███   ███    ███   ███    ███   ███    ███
        ███    ███ ▀█████████▀  ▀█████████▀  ▀███████████
        ███    ███   ███          ███          ███    ███
        ███    ███   ███          ███          ███    ███
         ▀██████▀   ▄████▀       ▄████▀        ███    █▀  """)

print('''###############################################################
#                    Welcome to OPPA-NET                      #
#                   Authorized access only!                   #
# Disconnect IMMEDIATELY if you are not an authorized user!!! #
#        All actions Will be monitored and recorded           #
###############################################################
''')

get_csrf_token()
login()
